import * as os from "node:os";
import * as fs from "node:fs/promises";
import * as path from "node:path";
import { Readable } from "node:stream";
import { on } from "node:events";
import { PrismaClient } from "@prisma/client";
const db = new PrismaClient();
import { faker } from "@faker-js/faker";

async function seed() {
  const users = Array.from({ length: 250 }, () => ({
    email: faker.internet.email(),
    name: faker.name.fullName(),
  }));
  for (const user of users) {
    await db.user.create({
      data: user,
    });
  }
}
seed();
