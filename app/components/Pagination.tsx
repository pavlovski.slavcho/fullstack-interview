import { Form } from '@remix-run/react';
import { useFetcher } from "@remix-run/react";

export default function Pagination(props) {

    const fetcher = useFetcher();
    const perPageOptions = [10,25,50,100];
    const perPage = props.perPage ? props.perPage : 10;
    const numPages = Math.ceil(props.total / perPage);
    const currentPage = props.currentPage ? parseInt(props.currentPage) : 1;

    function renderPagination(){
        let pages = [];

        let startPage = currentPage - 3;
        let endPage = currentPage + 3;

        if (startPage <= 0) {
            endPage -= (startPage - 1);
            startPage = 1;
        }

        if (endPage > numPages) {
            endPage = numPages;
        }

        if (startPage > 1) {
            pages.push(<li onClick={() => changePage(currentPage - 1)} className="cursor-pointer bg-gray-500 p-1 grow float-left inline-block mx-4 w-4 text-center"> Prev </li>);
        }

        if(((numPages - (endPage - 3)) < 4) && startPage > 2) {
            startPage = endPage - 6;
        }

        for (let i = startPage; i <= endPage; i++) {
            pages.push(<li onClick={() => changePage(i)} className={`cursor-pointer ${currentPage == i ? 'bg-gray-900' : 'bg-gray-500'} p-1 grow float-left inline-block mx-4 w-4 text-center`}>{i}</li>);
        }

        if(endPage < numPages) {
            pages.push(<li onClick={() => changePage(currentPage + 1)} className="cursor-pointer bg-gray-500 p-1 grow float-left inline-block mx-4 w-4 text-center"> Next </li>);
        }

        return pages.map((page)=>{ return page });
    }

    function changePage(page) {
        fetcher.submit({ page: page, search:props.search, perPage: perPage}, { method: "post"});
    }

    function changePerPage(newPage) {
        fetcher.submit({ perPage: newPage, search: props.search, page: 1}, { method: "post"});
    }

    function resetFilters() {
        fetcher.submit({ }, { method: "post"});
    }

    return (
        <Form method="get">
            <div className="mt-4 w-full grid grid-cols-3 gap-4">
                <input type="text" name="search" placeholder="Name or Email" defaultValue={props.search} className="text-black inline-block float-left"/>
                <input type="submit" value="Search" className="inline-block float-left"/>
                <input type="reset" value="Reset" className="inline-block float-left" onClick={resetFilters}/>
            </div>
            <div className="mt-4 w-full clear-both">
                <span>Per Page</span>
                <select value={props.perPage} name="limit" key="limit" className="text-black w-20" onChange={(e) => changePerPage(e.target.value)}>
                    {perPageOptions.map((option, index) => {
                        return <option value={option} key={`option-${index}`}>{option}</option>
                    })}
                </select>
            </div>
            <div className="mt-4 w-full">
                <ul className={`w-full flex list-none`}>
                    {renderPagination()}
                </ul>
            </div>
        </Form>
    )
}