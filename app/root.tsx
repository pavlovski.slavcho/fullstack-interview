import type { LinksFunction, MetaFunction } from "@remix-run/node";
import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
} from "@remix-run/react";
import watercss from "./water.css";

export const meta: MetaFunction = () => ({
  charset: "utf-8",
  title: "Fullstack Interview",
  viewport: "width=device-width,initial-scale=1",
});

export const links: LinksFunction = () => [
  { href: "https://cdn.tailwindcss.com/3.2.4", rel: "stylesheet" },
  { href: watercss, rel: "stylesheet" },
];

export default function App() {
  return (
    <html lang="en">
      <head>
        <Meta />
        <Links />
      <script src="https://cdn.tailwindcss.com?plugins=forms,typography,aspect-ratio,line-clamp"></script>
      </head>
      <body className="w-2/4 mx-auto">
        <Outlet />
        <ScrollRestoration />
        <Scripts />
        <LiveReload />
      </body>
    </html>
  );
}
