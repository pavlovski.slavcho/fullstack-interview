// Helpers from Remix
import { json, type LoaderArgs, ActionArgs, redirect } from "@remix-run/node";
import {getData} from "@remix-utils"
import { useLoaderData } from "@remix-run/react";
import Pagination from "../components/Pagination.tsx";

// The currently running Prisma Client
import db from "~/db.server";

// This function provides data to the Index component. It only runs on the server.
// The `request` parameter is an instance of [Request](https://developer.mozilla.org/en-US/docs/Web/API/Request)
// This function returns a [Response](https://developer.mozilla.org/en-US/docs/Web/API/Response) for the Index component to use
export async function loader({ request }: LoaderArgs) {
  // This fetches all the users from the database in ascending order of creation
  // https://www.prisma.io/docs/reference/api-reference/prisma-client-reference
  // The user object consists of:
  // {
  //    email: string
  //    name: string
  //    createdAt: Date
  //    updatedAt: Date
  // }
  let url = new URL(request.url);

  let params = {
      perPage: url.searchParams.get('perPage') ? parseInt(url.searchParams.get('perPage')) : 10,
      page: url.searchParams.get('page') ? parseInt(url.searchParams.get('page')) : 1,
      search: url.searchParams.get('search') ? url.searchParams.get('search') : ''
  };

  let search = {
    orderBy: {
      createdAt: "asc",
    },
    take: params.perPage,
  };

  if(params.page > 1) {
    search.skip = params.perPage * (params.page - 1)
  }

  if(params.search) {
    search.where = {
      OR: [
        {
          email: {
            contains: params.search,
          },
        },
        {
          name: {
            contains: params.search,
          },
        }
      ]
    };
  }

  const users = await db.user.findMany(search);
  search = Object.assign({}, search, {skip: undefined, take: undefined});
  const usersAll = await db.user.findMany(search)

  // This is just a helper function for sending the correct headers down https://remix.run/docs/en/v1/utils/json
  return json({ users, totalCount: usersAll.length, page:params.page, search:params.search, perPage:params.perPage});
}

export async function action({ request }: ActionArgs) {
  const params = await request.formData();
  return redirect('/?' + new URLSearchParams(params).toString())
}

// This is the main component that gets rendered on the page, through SSR and hydration
export default function Index() {
  // This parses the return of the loader function to use at render time
  const {users, totalCount, page, search, perPage} = useLoaderData<typeof action>();

  return (
    <div>
      <Pagination total={totalCount} currentPage={page} perPage={perPage} search={search} />
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <td>Email</td>
            <td>Created At</td>
          </tr>
        </thead>
        <tbody>
          {users.map((user) => (
            <tr key={user.id}>
              <td>{user.name}</td>
              <td>{user.email}</td>
              <td>
                <time dateTime={user.createdAt}>{user.createdAt}</time>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
